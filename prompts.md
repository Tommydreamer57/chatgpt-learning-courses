
# ChatGPT Learning Prompts

## Course Definition

Can you create a syllabus-style outline for learning _______ for someone with experience in HTML, CSS, JavaScript, and Postgres, and who is learning C++.

Please format all of your responses adhering to the following instructions:

- Write all responses in plain-text Markdown.
- Provide a title for each response using an H1 header.
- Organize lists into subsections with H2 headers.
- Embolden keywords
- Omit the "Title: " prefix and colon suffix from titles.
- Ensure there is an empty line between each H1/H2 and the following line.
- Number each section (H2) in the outline, but omit numbers in the sections of following responses

## Section Generation

Can you please explain items in section 1, following the provided formatting instructions?
